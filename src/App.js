import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import PrivateRoute from './components/PrivateRoute';
import Header from './components/Header';
import SignIn from './routes/SignIn';
import SignUp from './routes/SignUp';
import Video from './routes/Video';

const App = () => {

  return (
    <Router>
      <Header />
      <Switch>
        <Route exact path='/sign-in' component={SignIn} />
        <Route exact path='/sign-up' component={SignUp} />
        <PrivateRoute path='/' component={Video} />
      </Switch>
    </Router>
  );
}

export default App;
