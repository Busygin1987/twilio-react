import React from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import logo from '../../assets/img/Twilio-Logo.wine.svg';
import { logout } from '../../store/actions/auth';
import { selectUserAuth } from '../../store/selectors/auth';

const WrapHeader = styled.div`
  width: 100%;
  height: 80px;
  background-color: rgba(0, 0, 0, .8);
  background-image: url(${logo});
  background-position: left;
  background-repeat: no-repeat;
  background-size: 150px;
  position: relative;
  display: flex;
  align-items: center;
`;

const NavBlock = styled.div`
  position: absolute;
  right: 24px;
  display: flex;
  width: 130px;
  justify-content: space-between;
`;

const Header = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const authenticated = useSelector(selectUserAuth);

  const handleLogout = () => {
    dispatch(logout());
    history.replace('/');
  };

  return (
    <WrapHeader>
      <NavBlock>
        { authenticated ? (
          <div
            style={{
              color: '#fff',
              cursor: 'pointer'
            }}
            onClick={handleLogout}
          >LOGOUT</div>
        ) : (
          <>
            <NavLink to='/sign-in'
              activeStyle={{
                fontWeight: 'bold',
                color: 'red'
              }}
            >Sign In</NavLink>
            <NavLink to='/sign-up'
              activeStyle={{
                fontWeight: 'bold',
                color: 'red'
              }}
            >Sign Up</NavLink>
          </>
        ) }
      </NavBlock>
    </WrapHeader>
  );
}

export default Header;