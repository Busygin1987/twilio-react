import React from 'react';
import { Redirect, Route } from 'react-router-dom';

export default ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      localStorage.getItem('access_token') ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: '/sign-up',
            state: { from: props.location }
          }}
        />
      )
    }
  />
);
