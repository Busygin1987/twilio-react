import api from '../../services/api';

import * as types from './types';

const joinRoomRequest = () => ({ type: types.JOIN_ROOM_REQUEST });
const joinRoomSuccess = (payload) => ({ type: types.JOIN_ROOM_SUCCESS, payload });
const joinRoomError = (payload) => ({ type: types.JOIN_ROOM_ERROR, payload });

export const joinRoom = (roomId) => async (dispatch) => {
  dispatch(joinRoomRequest());
  try {
    const response =  await api('get', `/ws/room/${roomId}`);
    if (response.data.data.room) {
      dispatch(joinRoomSuccess(response.data.data));
    }
  } catch(error) {
    dispatch(joinRoomError(error));
  }
};

export const setUsername = (payload) => ({ type: types.SET_USERNAME, payload });
export const updateChatLog = (payload) => ({ type: types.UPDATE_CHAT_LOG, payload });

const twilioTokenRequest = () => ({ type: types.TWILIO_TOKEN_REQUEST });
const twilioTokenSuccess = (payload) => ({ type: types.GET_TWILIO_TOKEN_SUCCESS, payload });
const twilioTokenError = (payload) => ({ type: types.GET_TWILIO_TOKEN_ERROR, payload });

export const getTwilioToken = (roomId, identity) => async (dispatch) => {
  dispatch(twilioTokenRequest());
  try {
    const response = await api('get', `/twilio/token?room=${roomId}&identity=${identity}`);
    if (response.data) {
      dispatch(twilioTokenSuccess(response.data));
    }
  } catch(error) {
    dispatch(twilioTokenError(error));
  }
};

const twilioCreateRoomRequest = () => ({ type: types.TWILIO_CREATE_ROOM });
const twilioCreateRoomSuccess = (payload) => ({ type: types.TWILIO_CREATE_ROOM_SUCCESS, payload });
const twilioCreateRoomError = (payload) => ({ type: types.TWILIO_CREATE_ROOM_ERROR, payload });

export const createTwilioRoom = (name, type) => async (dispatch) => {
  dispatch(twilioCreateRoomRequest());
  try {
    const response = await api('post', '/twilio/room', { name, type });
    if (response.data?.room) {
      dispatch(twilioCreateRoomSuccess(response.data.room));
    } else if (response.data.message) {
      dispatch(twilioCreateRoomError(response.data.message));
    }
  } catch(error) {
    dispatch(twilioCreateRoomError(error));
  }
};

const twilioUpdateRoomRequest = () => ({ type: types.TWILIO_DELETE_ROOM });
const twilioUpdateRoomSuccess = () => ({ type: types.TWILIO_DELETE_ROOM_SUCCESS });
const twilioUpdateRoomError = (payload) => ({ type: types.TWILIO_DELETE_ROOM_ERROR, payload });

export const deleteTwilioRoom = (roomSid) => async (dispatch) => {
  dispatch(twilioUpdateRoomRequest());
  try {
    const response = await api('put', `/twilio/room/${roomSid}?status=completed`);
    if (response.status === 200) {
      dispatch(twilioUpdateRoomSuccess());
    }
  } catch(error) {
    dispatch(twilioUpdateRoomError(error));
  }
};


export const clearVideoChatError = () => ({ type: types.CLEAR_TWILIO_ERROR });