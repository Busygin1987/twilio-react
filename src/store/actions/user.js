import api from '../../services/api';
import { AsyncStorage } from 'react-native';

import * as types from './types';

const fetchUser = () => ({ type: types.FETCH_USER_DATA_REQUEST });
const successFetch = (payload) => ({ type: types.FETCH_USER_DATA_SUCCESS, payload });
const failureFetch = (payload) => ({ type: types.FETCH_USER_DATA_FAILURE, payload });

export const fetchUserDataAction = () => async (dispatch) => {
  dispatch(fetchUser());
  try {
    const response = await api('get', '/user');
    if (response.status === 200 && response.data.user) {
      dispatch(successFetch(response.data.user));
    }
  } catch (error) {
    if (error.response?.data?.message) {
      dispatch(failureFetch(error.response.data.message));
    } else {
      dispatch(failureFetch(error));
    }
  }
};

const fetchUsers = () => ({ type: types.FETCH_USERS_REQUEST });
const successFetchUsers = (payload) => ({ type: types.FETCH_USERS_SUCCESS, payload });
const failureFetchUsers = (payload) => ({ type: types.FETCH_USERS_FAILURE, payload });

export const fetchUsersDataAction = () => async (dispatch) => {
  dispatch(fetchUsers());
  try {
    const response = await api('get', '/user/all');
    if (response.status === 200 && response.data.users) {
      dispatch(successFetchUsers(response.data.users));
    }
  } catch (error) {
    if (error.response?.data?.message) {
      dispatch(failureFetchUsers(error.response.data.message));
    } else {
      dispatch(failureFetchUsers(error));
    }
  }
};

const fetchFriends = () => ({ type: types.FETCH_FRIENDS_REQUEST });
const successFetchFriends = (payload) => ({ type: types.FETCH_FRIENDS_SUCCESS, payload });
const failureFetchFriends = (payload) => ({ type: types.FETCH_FRIENDS_FAILURE, payload });

export const fetchUserFriendsAction = () => async (dispatch) => {
  dispatch(fetchFriends());
  try {
    const response = await api('get', '/user/friends');
    if (response.status === 200 && response.data.friends) {
      dispatch(successFetchFriends(response.data.friends));
    }
  } catch (error) {
    if (error.response?.data?.message) {
      dispatch(failureFetchFriends(error.response.data.message));
    } else {
      dispatch(failureFetchFriends(error));
    }
  }
};

const addFriend = () => ({ type: types.CREATE_NEW_FRIEND_REQUEST });
const successAddFriend = (payload) => ({ type: types.CREATE_NEW_FRIEND_SUCCESS, payload });
const failureAddFriend = (payload) => ({ type: types.CREATE_NEW_FRIEND_FAILURE, payload });

export const addNewFriendAction = (friend) => async (dispatch) => {
  dispatch(addFriend());
  try {
    const response = await api('post', '/user/friend', { friendId: friend.id });
    if (response.status === 200) {
      dispatch(successAddFriend({ ...friend, roomId: response.data.roomId }));
    }
  } catch (error) {
    if (error.response?.data?.message) {
      dispatch(failureAddFriend(error.response.data.message));
    } else {
      dispatch(failureAddFriend(error));
    }
  }
};

const deleteFriend = () => ({ type: types.DELETE_FRIEND_REQUEST });
const successDeleteFriend = (payload) => ({ type: types.DELETE_FRIEND_SUCCESS, payload });
const failureDeleteFriend = (payload) => ({ type: types.DELETE_FRIEND_FAILURE, payload });

export const deleteFriendAction = (friend) => async (dispatch) => {
  dispatch(deleteFriend());
  try {
    const response = await api('delete', `/user/friend/${friend.id}`);
    if (response.status === 204) {
      dispatch(successDeleteFriend(friend));
    }
  } catch (error) {
    if (error.response?.data?.message) {
      dispatch(failureDeleteFriend(error.response.data.message));
    } else {
      dispatch(failureDeleteFriend(error));
    }
  }
};

const createUser = () => ({ type: types.CREATE_USER_REQUEST });
const successCreate = (payload) => ({ type: types.CREATE_USER_SUCCESS, payload });
const failureCreate = (payload) => ({ type: types.CREATE_USER_FAILURE, payload });

export const createUserAction = (userData) => async (dispatch) => {
  dispatch(createUser());
  try {
    const { status, data } = await api('post', '/user', { user: userData });
    if (status === 200 && data.token) {
      const token = data.token.split(' ')[1];
      await AsyncStorage.setItem('access_token', token);
      dispatch(successCreate(data.user));
    }
  } catch (error) {
    if (error.response?.data?.message) {
      dispatch(failureCreate(error.response.data.message));
    } else {
      dispatch(failureCreate(error));
    }
  }
};

export const clearUserStore = () => async (dispatch) => {
  await AsyncStorage.removeItem('access_token');
  dispatch({ type: types.CLEAR_USER_STORE });
};
