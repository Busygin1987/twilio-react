import api from '../../services/api';
import jwtDecode from 'jwt-decode';

import {
  SIGN_UP_REQUEST,
  SIGN_UP_SUCCESS,
  SIGN_UP_FAILURE,
  SIGN_IN_REQUEST,
  SIGN_IN_SUCCESS,
  SIGN_IN_FAILURE,
  CLEAR_AUTH_ERRROR,
  LOGOUT_USER
} from './types';

const requestSignUp = () => ({ type: SIGN_UP_REQUEST });
const successSignUp = (payload) => ({ type: SIGN_UP_SUCCESS, payload });
const failureSignUp = (payload) => ({ type: SIGN_UP_FAILURE, payload });

export const signUpAction = (userData) => async (dispatch) => {
  dispatch(requestSignUp());
  try {
    const response = await api('post', '/auth/sign-up', userData);
    if (response.status === 200 && response.data.token) {
      const token = response.data.token.split(' ')[1];
      const decoded = jwtDecode(token);
      localStorage.setItem('access_token', token);
      localStorage.setItem('user_name', decoded.firstName);
      dispatch(successSignUp(decoded));
      return true;
    }
  } catch (error) {
    if (error.response?.data?.message) {
      dispatch(failureSignUp(error.response.data.message));
    } else {
      dispatch(failureSignUp(error));
    }
    return false;
  }
};

const requestSignIn = () => ({ type: SIGN_IN_REQUEST });
const successSignIn = (payload) => ({ type: SIGN_IN_SUCCESS, payload });
const failureSignIn = (payload) => ({ type: SIGN_IN_FAILURE, payload });

export const signInAction = (userData) => async (dispatch) => {
  dispatch(requestSignIn());
  try {
    const response = await api('post', '/auth/sign-in', userData);
    if (response.status === 200 && response.data.token) {
      const token = response.data.token.split(' ')[1];
      const decoded = jwtDecode(token);
      localStorage.setItem('access_token', token);
      localStorage.setItem('user_name', decoded.firstName);
      dispatch(successSignIn(decoded));
      return true;
    }
  } catch (error) {
    if (error.response?.data?.message) {
      dispatch(failureSignIn(error.response.data.message));
    } else {
      dispatch(failureSignIn(error));
    }
    return false;
  }
};

const logoutUser = () => ({ type: LOGOUT_USER });

export const logout = () => async (dispatch) => {
  localStorage.removeItem('access_token');
  localStorage.removeItem('user_name');
  dispatch(logoutUser());
};

export const clearError = () => ({ type: CLEAR_AUTH_ERRROR });

