import {
  SIGN_UP_REQUEST,
  SIGN_UP_SUCCESS,
  SIGN_UP_FAILURE,
  SIGN_IN_REQUEST,
  SIGN_IN_SUCCESS,
  SIGN_IN_FAILURE,
  LOGOUT_USER,
  CLEAR_AUTH_ERRROR
} from '../actions/types';

const initialState = {
  user: {},
  loading: false,
  error: null,
  authenticated: localStorage.getItem('access_token') ? true : false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SIGN_UP_REQUEST:
      return {
        ...state,
        loading: true
      };
    case SIGN_UP_SUCCESS:
      return {
        ...state,
        loading: false,
        user: action.payload,
        authenticated: true
      };
    case SIGN_UP_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    case SIGN_IN_REQUEST:
      return {
        ...state,
        loading: true
      };
    case SIGN_IN_SUCCESS:
      return {
        ...state,
        loading: false,
        user: action.payload,
        authenticated: true
      };
    case SIGN_IN_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    case LOGOUT_USER:
      return {
        ...state,
        user: {},
        authenticated: false
      }
    case CLEAR_AUTH_ERRROR:
      return {
        ...state,
        error: ''
      }
    default:
      return state;
  }
};