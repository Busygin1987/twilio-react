import * as types from '../actions/types';

const initialState = {
  room: null,
  chatLog: [],
  username: null,
  twilioToken: null,
  loading: false,
  error: null,
  twilioRoom: null
};

export default function chatReducer(state = initialState, action) {
  switch (action.type) {
    case types.JOIN_ROOM_REQUEST:
      return {
        ...state,
        loading: true
      };
    case types.JOIN_ROOM_SUCCESS:
      return {
        ...state,
        room: action.payload.room,
        chatLog: action.payload.chats,
        loading: false
      };
    case types.JOIN_ROOM_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    case types.SET_USERNAME:
      return {
        ...state,
        username: action.payload
      };
    case types.UPDATE_CHAT_LOG:
      if (state.room !== null && action.payload.room_id === state.room) {
        return {
          ...state,
          chatLog: [action.payload].concat(state.chatLog)
        };
      }
    // eslint-disable-next-line no-fallthrough
    case types.TWILIO_TOKEN_REQUEST:
      return {
        ...state,
        loading: true
      };
    case types.GET_TWILIO_TOKEN_SUCCESS:
      return {
        ...state,
        twilioToken: action.payload.token,
        loading: false
      };
    case types.GET_TWILIO_TOKEN_ERROR:
      return {
        ...state,
        error: action.payload,
        loading: false
      };
    case types.TWILIO_CREATE_ROOM:
      return {
        ...state,
        loading: true
      };
    case types.TWILIO_CREATE_ROOM_SUCCESS:
      return {
        ...state,
        twilioRoom: action.payload,
        loading: false
      };
    case types.TWILIO_CREATE_ROOM_ERROR:
      return {
        ...state,
        error: action.payload,
        loading: false
      };
    case types.TWILIO_DELETE_ROOM_SUCCESS:
      return {
        ...state,
        twilioRoom: null
      };
    case types.TWILIO_DELETE_ROOM_ERROR:
      return {
        ...state,
        error: action.payload
      };
    case types.CLEAR_TWILIO_ERROR:
      return {
        ...state,
        error: null
      };
    default:
      return state;
  }
}
