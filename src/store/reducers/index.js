import { combineReducers } from 'redux';

import auth from './auth';
import chat from './chat';
import user from './user';

export default combineReducers({
  auth,
  chat,
  me: user
});