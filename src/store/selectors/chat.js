import { createSelector } from 'reselect';

export const selectRoom = createSelector(
  state => state.chat,
  chat => chat.room
);

export const selectMessages = createSelector(
  state => state.chat,
  chat => chat.chatLog
);

export const selectLoading = createSelector(
  state => state.chat,
  chat => chat.loading
);

export const selectTwilioToken = createSelector(
  state => state.chat,
  chat => chat.twilioToken
);

export const selectTwilioRoom = createSelector(
  state => state.chat,
  chat => chat.twilioRoom
);

export const selectChatError = createSelector(
  state => state.chat,
  chat => chat.error
);
