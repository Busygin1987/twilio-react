import { createSelector } from 'reselect';

export const selectUserAuth = createSelector(
  state => state.auth,
  auth => auth.authenticated
);