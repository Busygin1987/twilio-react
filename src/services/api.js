import axios from 'axios';

export default async (method, url, data) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };

  const BASE_URL = 'https://twilio.devserver.app';

  const token = localStorage.getItem('access_token');
  if (token) headers.Authorization = `Bearer ${token}`;
  const ax = axios.create({
    baseURL: `${BASE_URL}/`,
    headers
  });

  return ax[method](url, data);
}