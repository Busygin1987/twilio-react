import React, { useState, useRef, useEffect } from 'react';
import styled from 'styled-components';
import { Button, FormGroup, Label, Input, Spinner } from 'reactstrap';
import { useSelector, useDispatch } from 'react-redux';
import { connect, createLocalTracks } from 'twilio-video';

import { getTwilioToken, createTwilioRoom, clearVideoChatError, deleteTwilioRoom } from '../../store/actions/chat';
import { selectChatError, selectTwilioRoom, selectTwilioToken, selectLoading } from '../../store/selectors/chat';

const FormWrapper = styled.div`
  border: 1px solid #333;
  border-radius: 10px;
  padding: 20px;
`;

const PageContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 100%;
`;

const Message = styled.div`
  font-size: 18px;
  margin: 15px;
  text-align: center;
  color: ${({ success }) => success ? '#228B22' : '#CD0000' };
`;

const LocalVideo = styled.div`
  display: flex;
  justify-content: center;
  height: 200px;
  width: 100%;
`;

const RemoteVideos = styled.div`
  display: flex;
  height: auto;
`;

const Video = () => {
  const dispatch = useDispatch();

  const token = useSelector(selectTwilioToken);
  const room = useSelector(selectTwilioRoom);
  const error = useSelector(selectChatError);
  const loading = useSelector(selectLoading);

  const [ roomName, setRoomName ] = useState('');
  const [ roomType, setRoomType ] = useState('group');
  const [ message, setMessage ] = useState('');

  const localVideoRef = useRef(null);
  const remoteVideoRef = useRef(null);

  const userName = localStorage.getItem('user_name');

  useEffect(() => {
    room && setMessage('Room was created success!');
    // return () => setMessage('');
  }, [room]);

  const addParticipant = participant => {
    console.log('Participant "%s" connected', participant.identity);

    const div = document.createElement('div');
    div.id = participant.sid;
    div.innerText = participant.identity;
    div.setAttribute('style', 'display: flex; flex-direction: column; align-items: center; font-weight: bold;');

    participant.on('trackSubscribed', track => {
      div.appendChild(track.attach());
      remoteVideoRef.current.appendChild(div);
    });

    participant.tracks.forEach((publication) => {
      if (publication.isSubscribed) {
        const track = publication.track;
        div.appendChild(track.attach());
        remoteVideoRef.current.appendChild(div);
      }
    });
  };

  useEffect(() => {
    let twilioRoom = null;
    const setConnect = async () => {
      try {
        const localTracks = await createLocalTracks({
          audio: true,
          video: { width: 400 }
        });

        twilioRoom = await connect(token, {
          tracks: localTracks,
          name: roomName
        });

        const [ audio, video ] = localTracks;
        localVideoRef.current.appendChild(video.attach());

        twilioRoom.on('participantConnected', addParticipant);

        twilioRoom.participants.forEach(addParticipant);

        twilioRoom.on('participantDisconnected', (participant) => {
          console.log(participant.identity + ' has disconnected');
        });

        twilioRoom.once('disconnected', async () => {
          if (room) {
            dispatch(deleteTwilioRoom(room.sid));
          }
          twilioRoom.localParticipant.tracks.forEach(publication => {
            const attachedElements = publication.track.detach();
            attachedElements.forEach(element => element.remove());
          });
          video.stop();
          console.log('You left the Room:', twilioRoom.name);
        });

        console.log('ROOM ', twilioRoom);
      } catch (error) {
        setMessage(error.message)
      }
    }

    token && setConnect();

    return () => twilioRoom && twilioRoom.disconnect();
  }, [ token ]);

  const createRoom = () => {
    error && dispatch(clearVideoChatError());
    roomName && dispatch(createTwilioRoom(roomName, roomType));
  };

  const connectRoom = () => {
    if (roomName) {
      dispatch(getTwilioToken(roomName, userName));
      dispatch(clearVideoChatError());
      setRoomName('');
      setMessage('');
    }
  };

  return (
    <PageContainer>
      { loading ? (
        <Spinner style={{ width: '3rem', height: '3rem' }} />
      ) : (
        !token ? (
          <FormWrapper>
            <FormGroup>
              <Label for='roomType'>Room type</Label>
              <Input
                type='select'
                name='roomType'
                onChange={(event) => setRoomType(event.target.value)}
                value={roomType}
              >
                <option label='P2P Room' value='peer-to-peer' />
                <option label='Small Group Room' value='group-small' />
                <option label='Group Room' value='group' />
              </Input>
            </FormGroup>
            <FormGroup >
              <Label for='roomName'>Room name</Label>
              <Input
                name='roomName'
                type='text'
                placeholder='Enter your room name'
                onChange={(event) => setRoomName(event.target.value)}
                value={roomName}
              />
            </FormGroup>
            <div style={{ display: 'flex', flexDirection: 'column', height: '90px', justifyContent: 'space-between' }}>
              <Button onClick={createRoom}  color='primary'>
                Create room
              </Button>
              <Button onClick={connectRoom} color='primary'>
                Connect room
              </Button>
            </div>
            {message && !(!!error) && <Message success>{message}</Message> }
            {error && <Message>{error}</Message> }
          </FormWrapper>
        ) : (
          <div style={{ width: '100%', height: '100vh' }}>
            <LocalVideo ref={localVideoRef} />
            <RemoteVideos ref={remoteVideoRef} />
          </div>
        )
      )}
    </PageContainer>
  );
}

export default Video;