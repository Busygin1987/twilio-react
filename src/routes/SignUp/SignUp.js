import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';
import { Button, FormGroup, Label, Input, Spinner } from 'reactstrap';
import { Form, Field } from 'react-final-form';

import { signUpAction } from '../../store/actions/auth';

import { validateEmail } from '../../utils/helpers';

const SignUp = ({ history }) => {
  const { loading, error } = useSelector(state => state.auth);
  const dispatch = useDispatch();

  const handleSubmit = async (values) => {
    const authenticated = await dispatch(signUpAction(values));

    authenticated && history.push('/video');
  };

  const validate = (values) => {
    const errors = {};

    if (!values.firstName) {
      errors.firstName = "Required";
    }
    if (!values.lastName) {
      errors.lastName = "Required";
    }
    if (!values.email) {
      errors.email = "Required";
    } else if (!validateEmail(values.email)) {
      errors.email = "Not an email adress";
    }
    if (!values.password) {
      errors.password = "Required";
    }
    if (!values.confirmPassword) {
      errors.confirmPassword = "Required";
    } else if (values.confirmPassword !== values.password) {
      errors.confirmPassword = "Does not match";
    }
    return errors;
  };

  const FormWrapper = styled.div`
    border: 1px solid #333;
    border-radius: 10px;
    padding: 20px;
  `;

  const PageContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    min-height: 100%;
  `;

  const styles = {
    label: {
      fontSize: '14px',
      fontWeight: 'bold'
    },
    input: {
      padding: '10px',
      margin: '15px 0 0 10px',
      fontSize: '14px',
      width: '70%'
    },
    group: {
      flexDirection: 'row',
      display: 'flex',
      alignItems: 'baseline',
      justifyContent: 'space-between'
    },
    button: {
      width: '100%',
      height: '50px',
      padding: '10px',
      marginBottom: '20px',
      fontSize: '18px'
    }
  };

  return (
    <PageContainer>
      { loading ? (
        <Spinner style={{ width: '3rem', height: '3rem' }} />
      ) : ( <FormWrapper>
        <Form
          onSubmit={handleSubmit}
          validate={validate}
          render={({ handleSubmit, values, submitting, validating, valid }) => (
            <form onSubmit={handleSubmit}>
              <FormGroup style={styles.group}>
                <Label style={styles.label} for="firstName">First Name</Label>
                <Field name="firstName">
                  {({ input, meta }) => (
                    <Input
                      {...input}
                      style={styles.input}
                      type="text"
                      placeholder="Enter your first name"
                      invalid={meta.error && meta.touched}
                    />
                  )}
                </Field>
              </FormGroup>
              <FormGroup style={styles.group}>
                <Label style={styles.label}  for="lastName">Last Name</Label>
                <Field name="lastName">
                  {({ input, meta }) => (
                    <Input
                      {...input}
                      style={styles.input}
                      type="text"
                      placeholder="Enter your last name"
                      invalid={meta.error && meta.touched}
                    />
                  )}
                </Field>
              </FormGroup>
              <FormGroup style={styles.group}>
                <Label style={styles.label}  for="email">Email</Label>
                <Field name="email">
                  {({ input, meta }) => (
                    <Input
                      {...input}
                      style={styles.input}
                      type="text"
                      placeholder="Enter your email"
                      invalid={meta.error && meta.touched}
                    />
                  )}
                </Field>
              </FormGroup>
              <FormGroup style={styles.group}>
                <Label style={styles.label}  for="password">Password</Label>
                <Field name="password">
                  {({ input, meta }) => (
                    <Input
                      {...input}
                      style={styles.input}
                      type="password"
                      placeholder="Enter your password"
                      invalid={meta.error && meta.touched}
                    />
                  )}
                </Field>
              </FormGroup>
              <FormGroup style={styles.group}>
                <Label style={styles.label}  for="confirmPassword">Confirm</Label>
                <Field name="confirmPassword">
                  {({ input, meta }) => (
                    <Input
                      {...input}
                      style={styles.input}
                      type="password"
                      placeholder="Confirm your password"
                      invalid={meta.error && meta.touched}
                    />
                  )}
                </Field>
              </FormGroup>
              <Button style={styles.button} color="primary" type="submit" disabled={!valid}>
                Submit
              </Button>
            </form>
          )}
        />
      </FormWrapper>)}
    </PageContainer>
  );
};

export default SignUp;
